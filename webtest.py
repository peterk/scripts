#!/usr/bin/env python

import requests
import sys
import signal
import argparse

#sites.txt contains a lists of hostnames one to a line

parser = argparse.ArgumentParser(description="A simple web checker.",
            epilog=" Pulls list of sites from sites.txt. One hostname per line.")
parser.add_argument('-err', action='store_true', dest='show_errors_only',
            help='show errors only', default=False)

args = parser.parse_args()

print args.show_errors_only

#def signal_handler(signal, frame):
#    sys.exit(0)

#signal.signal(signal.SIGINT, signal_handler)

sites = [line.strip() for line in open("sites.txt")]


BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE = range(8)


def printout(text, color=WHITE):
    seq = "\x1b[1;%dm" % (30 + color) + text + "\x1b[0m"
    sys.stdout.write(seq)

for site in sites:
    try:
        printout("Checking " + site + "\n")
        r = requests.get("http://" + site)
        if r.status_code > 200:
            printout("Error: ", RED)
        print "Status Code: " + str(r.status_code)
        #print "Status Code: " + str(r.headers['status'])
    except:
        printout("Could not connect to website\n", RED)
