#!/usr/bin/env python

import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument("filename", help="name (fullpath) of jsonfile to parse")
args = parser.parse_args()

def is_json(myjson):
    try:
        json_object = json.loads(myjson)
    except ValueError:
        return False
    return True

try:
    j = open(args.filename).read()
except IOError, e:
    print"problem reading file"
    raise

if is_json(j):
    print args.filename + " is valid json"
else:
    print args.filename + "is NOT valid json"
